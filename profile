## Easy Gentoo - Example Profile ##

## Note: if you want to use a default value, then you don't have to specify it

## boot          partition    label
boot             sda2         Boot

## swap          partition    label
swap             sda3         Swap

## root          partition    label    filesystem
root             sda4         Root     ext4

## grub          where to install grub (hdc, sdb, sda3...) (none=disabled) - default: root partition
## grub          none
grub             sda

## setup         enable/disable audio/video codec USE flags (disabled for basic, enabled for normal) - default: basic
setup            normal

## domainname    domainname to use - default: easygentoo
domainname       grimmig.mine.nu

## rootpass      root password - default: toor
rootpass         Tbr3Va

## username      your username - default: owner
username         chuck

## userpass      your user password - default: resu
userpass         kv693eRx

